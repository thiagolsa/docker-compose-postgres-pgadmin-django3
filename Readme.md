Repositorio com docker-composer.yml para postgresql 11 e pgadmin lasted version.

Start in standalone mode:
docker-compose run --service-ports --rm django /bin/bash

## Help's
Acesse psql prompt
docker exec -it postgres psql -U postgres
Make a dump
docker exec <postgres_container_name> pg_dump -U postgres <database_name> > backup.sql
E.G.
docker exec docker-composer_postgres-db_1 pg_dump -U teste teste > backup.sql
Make a restau
docker exec -i <postgres_container_name> psql -U postgres -d <database_name> < backup.sql
E.G.
docker exec -i docker-composer_postgres-db_1 psql -U teste -d teste < Cloud_SQL_Export_2020-02-22.dump


Referencias

Docker-compose:
https://medium.com/@renato.groffe/postgresql-pgadmin-4-docker-compose-montando-rapidamente-um-ambiente-para-uso-55a2ab230b89